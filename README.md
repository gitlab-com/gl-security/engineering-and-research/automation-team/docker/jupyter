# jupyter

This project's CI pipeline builds and pushes a [Jupyter Docker Stacks](https://jupyter-docker-stacks.readthedocs.io/en/latest/index.html) image into the GitLab Container Registry.

It can be pulled for use in Security Automation ML projects to provide a containerized JupyterLab environment to enable one to code and run training experiments. It enables you to run any of the Python files or notebooks within this repository on JupyterLab, and includes common python ML libraries e.g numpy, pandas, tensorflow.

## Steps to run

1. Create a script with the code below:

   ```bash
    #!/bin/bash -e

    PORT=${PORT:-8888}

    docker start IMAGE_NAME ||
        docker run -d  \
        --name IMAGE_NAME \
        -e CHOWN_HOME=yes \
        -e CHOWN_HOME_OPTS='-R' \
        -e JUPYTER_ENABLE_LAB=yes \
        -e NB_UID="$(id -u)" \
        -e NB_GID="$(id -g)" \
        -e NB_USER="${USER}" \
        -p 127.0.0.1:"${PORT}:${PORT}" \
        -u root \
        -v "${PWD}":"/home/${USER}/work" \
        -w "/home/${USER}" \
        registry.gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/docker/jupyter:TAG \
        start-notebook.sh \
        --port "$PORT"
    until URL=$(docker logs IMAGE_NAME 2>&1 | egrep -m1 -o "http://127.0.0.1:${PORT}/lab\?token=[0-9a-f]+"); do
        sleep 1
    done
    echo "Jupyter URL: $URL"
    ```

    Replace `IMAGE_NAME` with the preferred name you would like to assign to the container.

    Note:
    By default JupyterLab starts at port 8888. If you're running multiple projects on different JupyterLab container installations, you'd need to use custom port configurations for each installation. You can do this by overriding the `PORT` value e.g: `PORT=8002 ./jupyter-run.sh`

2. Run this script at the base of any Security Automation ML project. It should start a container and provide you with a Jupyter notebook server URL to open on a browser and access the JupyterLab application.

3. If there are any changes you would like to make to your project Pipfile, you can activate a `pipenv` shell in the JupyterLab terminal/docker container shell and `pipenv install/update` packages from there.


## Updating image tag

The image is tagged based on the TensorFlow version in `requirements.txt` e.g. `tensorflow-2.11`. Whenever the tensorflow package version is updated, remember to also update the tag in `.gitlab-ci.yml` to the new version.
